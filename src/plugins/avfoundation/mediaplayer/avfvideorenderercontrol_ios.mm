/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtMultimedia/qabstractvideosurface.h>
#include <QtMultimedia/qvideosurfaceformat.h>
#include "avfvideorenderercontrol_ios.h"
#include "avfvideosampleprovider_ios.h"
#include "avfscreenlink.h"

#import <AVFoundation/AVFoundation.h>

QT_USE_NAMESPACE

class QImageVideoBuffer3 : public QAbstractVideoBuffer
{
public:
    QImageVideoBuffer3(const QImage &image)
        : QAbstractVideoBuffer(NoHandle)
        , m_image(image)
        , m_mode(NotMapped)
    {

    }

    MapMode mapMode() const { return m_mode; }
    uchar *map(MapMode mode, int *numBytes, int *bytesPerLine)
    {
        if (mode != NotMapped && m_mode == NotMapped) {
            m_mode = mode;
            *numBytes = m_image.byteCount();
            *bytesPerLine = m_image.bytesPerLine();
            return m_image.bits();
        } else
            return 0;

    }

    void unmap() {
        m_mode = NotMapped;
    }
private:
    QImage m_image;
    MapMode m_mode;
};

AVFVideoRendererControl_iOS::AVFVideoRendererControl_iOS(QObject *parent)
    : QVideoRendererControl(parent)
    , m_surface(0)
    , start(0)

{
    m_link = new AVFScreenLink(this);
    connect(m_link, SIGNAL(tick(qint64)), this, SLOT(renderSample(qint64)));
}

AVFVideoRendererControl_iOS::~AVFVideoRendererControl_iOS()
{

}

QAbstractVideoSurface *AVFVideoRendererControl_iOS::surface() const
{
    return m_surface;
}

void AVFVideoRendererControl_iOS::setSurface(QAbstractVideoSurface *surface)
{
    if (surface == m_surface)
        return;

    QMutexLocker locker(&m_mutex);

    if (m_surface && m_surface->isActive())
        m_surface->stop();

    m_surface = surface;

    //If the surface changed, then the current frame renderer is no longer valid
//    if (m_frameRenderer)
//        delete m_frameRenderer;

    //If there is now no surface to render too
    if (m_surface == 0) {
        m_link->stop();
        return;
    }

    //Surface changed, so we need a new frame renderer
//    m_frameRenderer = new AVFVideoFrameRenderer(m_surface, this);

    //Check for needed formats to render as OpenGL Texture
//    m_enableOpenGL = m_surface->supportedPixelFormats(QAbstractVideoBuffer::GLTextureHandle).contains(QVideoFrame::Format_BGR32);

    //If we already have a layer, but changed surfaces start rendering again
    if (m_provider && !m_link->isActive()) {
        m_link->start();
    }
}

void AVFVideoRendererControl_iOS::setSampleProvider(AVFVideoSampleProvider_iOS* provider)
{
    if (m_provider == provider)
        return;

    m_provider = provider;

    //If there is an active surface, make sure it has been stopped so that
    //we can update it's state with the new content.
    if (m_surface && m_surface->isActive())
        m_surface->stop();

    //If there is no layer to render, stop scheduling updates
    if (m_provider == 0) {
        m_link->stop();
        QImage temp(QSize(1080, 1980), QImage::Format_ARGB32);
        present(temp);
        return;
    }

//    setupVideoOutput();

    //If we now have both a valid surface and layer, start scheduling updates
    if (m_surface && !m_link->isActive()) {
        m_link->start();
    }




    //fallback to rendering frames to QImages



//    CMSampleBufferRef sampleBuffer = m_provider->readNextSample();

//    if(sampleBuffer == 0) {
//        return;
//    }

//    qDebug() << "SAMPLES: " << CMSampleBufferGetNumSamples(sampleBuffer);
//    CMTime time = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
//    qDebug() << "TIMESTAMP: " << time.value / time.timescale;

//    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
//    CFRelease(sampleBuffer);

//    CVPixelBufferLockBaseAddress(imageBuffer,0);

//    unsigned char *pixels = ( unsigned char *)CVPixelBufferGetBaseAddress(imageBuffer);
//    int width = CVPixelBufferGetWidth(imageBuffer);
//    int height = CVPixelBufferGetHeight(imageBuffer);
//    int bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);


//    QImage frameData(pixels, width, height, bytesPerRow, QImage::Format_RGB32);

//    if (frameData.isNull()) {
//        qWarning("renterLayerToImage failed");
//        return;
//    }

//    QAbstractVideoBuffer *buffer = new QImageVideoBuffer3(frameData);
//    QVideoFrame frame = QVideoFrame(buffer, QSize(480, 640), QVideoFrame::Format_ARGB32);

//    if (m_surface && frame.isValid()) {
//        if (m_surface->isActive() && m_surface->surfaceFormat().pixelFormat() != frame.pixelFormat())
//            m_surface->stop();

//        if (!m_surface->isActive()) {
//            QVideoSurfaceFormat format(frame.size(), frame.pixelFormat(), QAbstractVideoBuffer::NoHandle);

//            if (!m_surface->start(format)) {
//                qWarning("Failed to activate video surface");
//            }
//        }

//        if (m_surface->isActive())
//            m_surface->present(frame);
//    }

}

void AVFVideoRendererControl_iOS::present(const QImage &image)
{
    if (image.isNull()) {
        qWarning("renterLayerToImage failed");
        return;
    }

    QAbstractVideoBuffer *buffer = new QImageVideoBuffer3(image);
    QVideoFrame frame = QVideoFrame(buffer, image.size(), QVideoFrame::Format_ARGB32);

    if (m_surface && frame.isValid()) {
        if (m_surface->isActive() && m_surface->surfaceFormat().pixelFormat() != frame.pixelFormat())
            m_surface->stop();

        if (!m_surface->isActive()) {
            QVideoSurfaceFormat format(frame.size(), frame.pixelFormat(), QAbstractVideoBuffer::NoHandle);

            if (!m_surface->start(format)) {
                qWarning("Failed to activate video surface");
            }
        }

        if (m_surface->isActive())
            m_surface->present(frame);
    }

}

void AVFVideoRendererControl_iOS::renderSample(qint64 timestamp)
{
    CMSampleBufferRef sampleBuffer = m_provider->readNextSample();

    if(sampleBuffer == 0) {
        start = 0;
        return;
    }

    if (start == 0)
    {
        start = timestamp;
    }

    CMTime time = m_provider->lastTime();

    qint64 t1 = timestamp - start;
    qint64 t2 = 1000 * time.value / time.timescale;
    if (t1 < t2)
    {
        return;
    }


    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);

    CVPixelBufferLockBaseAddress(imageBuffer,0);

    unsigned char *pixels = ( unsigned char *)CVPixelBufferGetBaseAddress(imageBuffer);
    int width = CVPixelBufferGetWidth(imageBuffer);
    int height = CVPixelBufferGetHeight(imageBuffer);
    int bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);

    QImage frameData(pixels, width, height, bytesPerRow, QImage::Format_ARGB32);
    QImage data = frameData;
    data.detach();

    if (data.isNull()) {
//            qWarning("renterLayerToImage failed");
        return;
    }

    present(data);

    CFRelease(sampleBuffer);

    m_provider->release();
}
