/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "avfmediaplayersession_mac.h"
#include "avfmediaplayerservice.h"

QT_USE_NAMESPACE

AVFMediaPlayerSession::AVFMediaPlayerSession(AVFMediaPlayerService *service, QObject *parent)
    : QObject(parent)
    , m_service(service)
    , m_state(QMediaPlayer::StoppedState)
    , m_mediaStatus(QMediaPlayer::NoMedia)
    , m_mediaStream(0)
    , m_muted(false)
    , m_tryingAsync(false)
    , m_volume(100)
    , m_rate(1.0)
    , m_duration(0)
    , m_videoAvailable(false)
    , m_audioAvailable(false)
{

}

AVFMediaPlayerSession::~AVFMediaPlayerSession()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession::setVideoOutput(QObject *output)
{
    internalSetVideoOutput(output);
}

void *AVFMediaPlayerSession::currentAssetHandle()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return internalAssentHandle();
}

QMediaPlayer::State AVFMediaPlayerSession::state() const
{
    return m_state;
}

QMediaPlayer::MediaStatus AVFMediaPlayerSession::mediaStatus() const
{
    return m_mediaStatus;
}

QMediaContent AVFMediaPlayerSession::media() const
{
    return m_resources;
}

const QIODevice *AVFMediaPlayerSession::mediaStream() const
{
    return m_mediaStream;
}

void AVFMediaPlayerSession::setMedia(const QMediaContent &content, QIODevice *stream)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << content.canonicalUrl();
#endif

    m_resources = content;
    m_mediaStream = stream;

    setAudioAvailable(false);
    setVideoAvailable(false);

    QMediaPlayer::MediaStatus oldMediaStatus = m_mediaStatus;

    if (content.isNull() || content.canonicalUrl().isEmpty()) {
        internalUnloadMedia();
        m_mediaStatus = QMediaPlayer::NoMedia;
        if (m_state != QMediaPlayer::StoppedState)
            Q_EMIT stateChanged(m_state = QMediaPlayer::StoppedState);

        if (m_mediaStatus != oldMediaStatus)
            Q_EMIT mediaStatusChanged(m_mediaStatus);
        Q_EMIT positionChanged(position());
        return;
    } else {

        m_mediaStatus = QMediaPlayer::LoadingMedia;
        if (m_mediaStatus != oldMediaStatus)
            Q_EMIT mediaStatusChanged(m_mediaStatus);
    }
    internalLoadMedia(content.canonicalUrl());
}

qint64 AVFMediaPlayerSession::position() const
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return internalPosition();
}

qint64 AVFMediaPlayerSession::duration() const
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return internalDuration();
}

int AVFMediaPlayerSession::bufferStatus() const
{
    //BUG: bufferStatus may be relevant?
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return 100;
}

int AVFMediaPlayerSession::volume() const
{
    return m_volume;
}

bool AVFMediaPlayerSession::isMuted() const
{
    return m_muted;
}

void AVFMediaPlayerSession::setAudioAvailable(bool available)
{
    if (m_audioAvailable == available)
        return;

    m_audioAvailable = available;
    Q_EMIT audioAvailableChanged(available);
}

bool AVFMediaPlayerSession::isAudioAvailable() const
{
    return m_audioAvailable;
}

void AVFMediaPlayerSession::setVideoAvailable(bool available)
{
    if (m_videoAvailable == available)
        return;

    m_videoAvailable = available;
    Q_EMIT videoAvailableChanged(available);
}

bool AVFMediaPlayerSession::isVideoAvailable() const
{
    return m_videoAvailable;
}

bool AVFMediaPlayerSession::isSeekable() const
{
    return true;
}

QMediaTimeRange AVFMediaPlayerSession::availablePlaybackRanges() const
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return internalPlaybackRanges();
}

qreal AVFMediaPlayerSession::playbackRate() const
{
    return m_rate;
}

void AVFMediaPlayerSession::setPlaybackRate(qreal rate)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << rate;
#endif

    if (qFuzzyCompare(m_rate, rate))
        return;

    m_rate = rate;

    if (m_state == QMediaPlayer::PlayingState) {
        internalSetRate(m_rate);
    }
}

void AVFMediaPlayerSession::setPosition(qint64 pos)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << pos;
#endif

    if ( !isSeekable() || pos == position())
        return;

    if (duration() > 0)
        pos = qMin(pos, duration());

    internalSetPosition(pos);

    //reset the EndOfMedia status position is changed after playback is finished
    if (m_mediaStatus == QMediaPlayer::EndOfMedia)
        processLoadStateChange();
}

void AVFMediaPlayerSession::play()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << "currently: " << m_state;
#endif

    if (m_state == QMediaPlayer::PlayingState)
        return;

    m_state = QMediaPlayer::PlayingState;

    //reset the EndOfMedia status if the same file is played again
    if (m_mediaStatus == QMediaPlayer::EndOfMedia) {
        setPosition(0);
        processLoadStateChange();
    }

    if (m_mediaStatus == QMediaPlayer::LoadedMedia || m_mediaStatus == QMediaPlayer::BufferedMedia)
        internalPlay();

    //processLoadStateChange();
    Q_EMIT stateChanged(m_state);
}

void AVFMediaPlayerSession::pause()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << "currently: " << m_state;
#endif

    if (m_state == QMediaPlayer::PausedState)
        return;

    m_state = QMediaPlayer::PausedState;

    //reset the EndOfMedia status if the same file is played again
    if (m_mediaStatus == QMediaPlayer::EndOfMedia)
        processLoadStateChange();

    internalPause();

    //processLoadStateChange();
    Q_EMIT stateChanged(m_state);
}

void AVFMediaPlayerSession::stop()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << "currently: " << m_state;
#endif

    if (m_state == QMediaPlayer::StoppedState)
        return;

    m_state = QMediaPlayer::StoppedState;
    m_rate = 0.0f;
    internalStop();
    setPosition(0);

    processLoadStateChange();
    Q_EMIT stateChanged(m_state);
    Q_EMIT positionChanged(position());
}

void AVFMediaPlayerSession::setVolume(int volume)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << volume;
#endif

    if (m_volume == volume)
        return;

    m_volume = volume;

    internalSetVolume(volume);
    Q_EMIT volumeChanged(m_volume);
}

void AVFMediaPlayerSession::setMuted(bool muted)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << muted;
#endif
    if (m_muted == muted)
        return;

    m_muted = muted;
    internalSetMuted(m_muted);
    Q_EMIT mutedChanged(muted);
}

void AVFMediaPlayerSession::processEOS()
{
    //AVPlayerItem has reached end of track/stream
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    Q_EMIT positionChanged(position());
    m_mediaStatus = QMediaPlayer::EndOfMedia;

    Q_EMIT stateChanged(m_state = QMediaPlayer::StoppedState);
    Q_EMIT mediaStatusChanged(m_mediaStatus);
}

void AVFMediaPlayerSession::processLoadStateChange()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << currentStatus;
#endif

    QMediaPlayer::MediaStatus newStatus = QMediaPlayer::NoMedia;
    bool isPlaying = (m_state != QMediaPlayer::StoppedState);

    if (internalProcessLoadStateChange())
    {
        qint64 currentDuration = duration();
        if (m_duration != currentDuration)
            Q_EMIT durationChanged(m_duration = currentDuration);

        newStatus = isPlaying ? QMediaPlayer::BufferedMedia : QMediaPlayer::LoadedMedia;

    }

    if (newStatus != m_mediaStatus)
        Q_EMIT mediaStatusChanged(m_mediaStatus = newStatus);

}

void AVFMediaPlayerSession::processPositionChange()
{
    Q_EMIT positionChanged(position());
}

void AVFMediaPlayerSession::processMediaLoadError()
{
    Q_EMIT error(QMediaPlayer::FormatError, tr("Failed to load media"));
    Q_EMIT mediaStatusChanged(m_mediaStatus = QMediaPlayer::InvalidMedia);
    Q_EMIT stateChanged(m_state = QMediaPlayer::StoppedState);
}
