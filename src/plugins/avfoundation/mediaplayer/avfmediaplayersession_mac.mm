/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "avfmediaplayersession_mac.h"
#include "avfmediaplayerservice.h"
#include "avfvideooutput.h"

#import <AVFoundation/AVFoundation.h>

QT_USE_NAMESPACE

//AVAsset Keys
static NSString* const AVF_TRACKS_KEY       = @"tracks";
static NSString* const AVF_PLAYABLE_KEY     = @"playable";

//AVPlayerItem keys
static NSString* const AVF_STATUS_KEY       = @"status";

//AVPlayer keys
static NSString* const AVF_RATE_KEY         = @"rate";
static NSString* const AVF_CURRENT_ITEM_KEY = @"currentItem";

static void *AVFMediaPlayerSessionObserverRateObservationContext = &AVFMediaPlayerSessionObserverRateObservationContext;
static void *AVFMediaPlayerSessionObserverStatusObservationContext = &AVFMediaPlayerSessionObserverStatusObservationContext;
static void *AVFMediaPlayerSessionObserverCurrentItemObservationContext = &AVFMediaPlayerSessionObserverCurrentItemObservationContext;

@interface AVFMediaPlayerSessionObserver : NSObject
{
@private
    AVFMediaPlayerSession *m_session;
    AVPlayer *m_player;
    AVPlayerItem *m_playerItem;
    AVPlayerLayer *m_playerLayer;
    NSURL *m_URL;
}

@property (readonly, getter=player) AVPlayer* m_player;
@property (readonly, getter=playerItem) AVPlayerItem* m_playerItem;
@property (readonly, getter=playerLayer) AVPlayerLayer* m_playerLayer;
@property (readonly, getter=session) AVFMediaPlayerSession* m_session;

- (AVFMediaPlayerSessionObserver *) initWithMediaPlayerSession:(AVFMediaPlayerSession *)session;
- (void) setURL:(NSURL *)url;
- (void) unloadMedia;
- (void) prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys;
- (void) assetFailedToPrepareForPlayback:(NSError *)error;
- (void) playerItemDidReachEnd:(NSNotification *)notification;
- (void) playerItemTimeJumped:(NSNotification *)notification;
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                         change:(NSDictionary *)change context:(void *)context;
- (void) detatchSession;
- (void) dealloc;
@end

@implementation AVFMediaPlayerSessionObserver

@synthesize m_player, m_playerItem, m_playerLayer, m_session;

- (AVFMediaPlayerSessionObserver *) initWithMediaPlayerSession:(AVFMediaPlayerSession *)session
{
    if (!(self = [super init]))
        return nil;

    self->m_session = session;
    return self;
}

- (void) setURL:(NSURL *)url
{
    if (m_URL != url)
    {
        [m_URL release];
        m_URL = [url copy];

        //Create an asset for inspection of a resource referenced by a given URL.
        //Load the values for the asset keys "tracks", "playable".

        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:m_URL options:nil];
        NSArray *requestedKeys = [NSArray arrayWithObjects:AVF_TRACKS_KEY, AVF_PLAYABLE_KEY, nil];

        // Tells the asset to load the values of any of the specified keys that are not already loaded.
        [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{
             dispatch_async( dispatch_get_main_queue(),
                           ^{
                                [self prepareToPlayAsset:asset withKeys:requestedKeys];
                            });
         }];
    }
}

- (void) unloadMedia
{
    if (m_player)
        [m_player setRate:0.0];
    if (m_playerItem) {
        [m_playerItem removeObserver:self forKeyPath:AVF_STATUS_KEY];

        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:m_playerItem];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemTimeJumpedNotification
                                                    object:m_playerItem];
        m_playerItem = 0;
    }
}

- (void) prepareToPlayAsset:(AVURLAsset *)asset
                   withKeys:(NSArray *)requestedKeys
{
    //Make sure that the value of each key has loaded successfully.
    for (NSString *thisKey in requestedKeys)
    {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
#ifdef QT_DEBUG_AVF
        qDebug() << Q_FUNC_INFO << [thisKey UTF8String] << " status: " << keyStatus;
#endif
        if (keyStatus == AVKeyValueStatusFailed)
        {
            [self assetFailedToPrepareForPlayback:error];
            return;
        }
    }

    //Use the AVAsset playable property to detect whether the asset can be played.
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << "isPlayable: " << [asset isPlayable];
#endif
    if (!asset.playable)
    {
        //Generate an error describing the failure.
        NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
        NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
        NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
                localizedDescription, NSLocalizedDescriptionKey,
                localizedFailureReason, NSLocalizedFailureReasonErrorKey,
                nil];
        NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];

        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];

        return;
    }

    //At this point we're ready to set up for playback of the asset.
    //Stop observing our prior AVPlayerItem, if we have one.
    if (m_playerItem)
    {
        //Remove existing player item key value observers and notifications.
        [self unloadMedia];
    }

    //Create a new instance of AVPlayerItem from the now successfully loaded AVAsset.
    m_playerItem = [AVPlayerItem playerItemWithAsset:asset];

    //Observe the player item "status" key to determine when it is ready to play.
    [m_playerItem addObserver:self
                   forKeyPath:AVF_STATUS_KEY
                      options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                      context:AVFMediaPlayerSessionObserverStatusObservationContext];

    //When the player item has played to its end time we'll toggle
    //the movie controller Pause button to be the Play button
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:m_playerItem];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemTimeJumped:)
                                                 name:AVPlayerItemTimeJumpedNotification
                                               object:m_playerItem];


    //Clean up old player if we have one
    if (m_player) {
        [m_player setRate:0.0];
        [m_player removeObserver:self forKeyPath:AVF_CURRENT_ITEM_KEY];
        [m_player removeObserver:self forKeyPath:AVF_RATE_KEY];
        [m_player release];
        m_player = 0;

        if (m_playerLayer) {
            [m_playerLayer release];
            m_playerLayer = 0; //Will have been released
        }
    }

    //Get a new AVPlayer initialized to play the specified player item.
    m_player = [AVPlayer playerWithPlayerItem:m_playerItem];
    [m_player retain];

#if defined(Q_OS_OSX)
    //Set the initial volume on new player object
    if (self.session)
        m_player.volume = m_session->volume() / 100.0f;
#endif

    //Create a new player layer if we don't have one already
    if (!m_playerLayer)
    {
        m_playerLayer = [AVPlayerLayer playerLayerWithPlayer:m_player];
        [m_playerLayer retain];
        m_playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        m_playerLayer.anchorPoint = CGPointMake(0.0f, 0.0f);
    }

    //Observe the AVPlayer "currentItem" property to find out when any
    //AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did
    //occur.
    [m_player addObserver:self
                          forKeyPath:AVF_CURRENT_ITEM_KEY
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:AVFMediaPlayerSessionObserverCurrentItemObservationContext];

    //Observe the AVPlayer "rate" property to update the scrubber control.
    [m_player addObserver:self
                          forKeyPath:AVF_RATE_KEY
                          options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                          context:AVFMediaPlayerSessionObserverRateObservationContext];

}

-(void) assetFailedToPrepareForPlayback:(NSError *)error
{
    Q_UNUSED(error)
    QMetaObject::invokeMethod(m_session, "processMediaLoadError", Qt::AutoConnection);
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
    qDebug() << [[error localizedDescription] UTF8String];
    qDebug() << [[error localizedFailureReason] UTF8String];
    qDebug() << [[error localizedRecoverySuggestion] UTF8String];
#endif
}

- (void) playerItemDidReachEnd:(NSNotification *)notification
{
    Q_UNUSED(notification)
    if (self.session)
        QMetaObject::invokeMethod(m_session, "processEOS", Qt::AutoConnection);
}

- (void) playerItemTimeJumped:(NSNotification *)notification
{
    Q_UNUSED(notification)
    if (self.session)
        QMetaObject::invokeMethod(m_session, "processPositionChange", Qt::AutoConnection);
}

- (void) observeValueForKeyPath:(NSString*) path
                       ofObject:(id)object
                         change:(NSDictionary*)change
                        context:(void*)context
{
    //AVPlayerItem "status" property value observer.
    if (context == AVFMediaPlayerSessionObserverStatusObservationContext)
    {
        AVPlayerStatus status = (AVPlayerStatus)[[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
            //Indicates that the status of the player is not yet known because
            //it has not tried to load new media resources for playback
            case AVPlayerStatusUnknown:
            {
                //QMetaObject::invokeMethod(m_session, "processLoadStateChange", Qt::AutoConnection);
            }
            break;

            case AVPlayerStatusReadyToPlay:
            {
                //Once the AVPlayerItem becomes ready to play, i.e.
                //[playerItem status] == AVPlayerItemStatusReadyToPlay,
                //its duration can be fetched from the item.
                if (self.session)
                    QMetaObject::invokeMethod(m_session, "processLoadStateChange", Qt::AutoConnection);
            }
            break;

            case AVPlayerStatusFailed:
            {
                AVPlayerItem *playerItem = (AVPlayerItem *)object;
                [self assetFailedToPrepareForPlayback:playerItem.error];

                if (self.session)
                    QMetaObject::invokeMethod(m_session, "processLoadStateChange", Qt::AutoConnection);
            }
            break;
        }
    }
    //AVPlayer "rate" property value observer.
    else if (context == AVFMediaPlayerSessionObserverRateObservationContext)
    {
        //QMetaObject::invokeMethod(m_session, "setPlaybackRate",  Qt::AutoConnection, Q_ARG(qreal, [m_player rate]));
    }
    //AVPlayer "currentItem" property observer.
    //Called when the AVPlayer replaceCurrentItemWithPlayerItem:
    //replacement will/did occur.
    else if (context == AVFMediaPlayerSessionObserverCurrentItemObservationContext)
    {
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        if (m_playerItem != newPlayerItem)
            m_playerItem = newPlayerItem;
    }
    else
    {
        [super observeValueForKeyPath:path ofObject:object change:change context:context];
    }
}

- (void) detatchSession
{
#ifdef QT_DEBUG_AVF
        qDebug() << Q_FUNC_INFO;
#endif
        m_session = 0;
}

- (void) dealloc
{
#ifdef QT_DEBUG_AVF
        qDebug() << Q_FUNC_INFO;
#endif
    [self unloadMedia];

    if (m_player) {
        [m_player removeObserver:self forKeyPath:AVF_CURRENT_ITEM_KEY];
        [m_player removeObserver:self forKeyPath:AVF_RATE_KEY];
        [m_player release];
        m_player = 0;
    }

    if (m_playerLayer) {
        [m_playerLayer release];
        m_playerLayer = 0;
    }

    if (m_URL) {
        [m_URL release];
    }

    [super dealloc];
}

@end

AVFMediaPlayerSession_Mac::AVFMediaPlayerSession_Mac(AVFMediaPlayerService *service, QObject *parent)
    : AVFMediaPlayerSession(service, parent)
    , m_videoOutput(0)
{
    m_observer = [[AVFMediaPlayerSessionObserver alloc] initWithMediaPlayerSession:this];
}

AVFMediaPlayerSession_Mac::~AVFMediaPlayerSession_Mac()
{
    //Detatch the session from the sessionObserver (which could still be alive trying to communicate with this session).
    [(AVFMediaPlayerSessionObserver*)m_observer detatchSession];
    [(AVFMediaPlayerSessionObserver*)m_observer release];
}

void AVFMediaPlayerSession_Mac::internalSetVideoOutput(QObject *output)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << output;
#endif

    if (m_videoOutput == qobject_cast<AVFVideoOutput*>(output))
        return;

    //Set the current output layer to null to stop rendering
    if (m_videoOutput) {
        m_videoOutput->setLayer(0);
    }

    m_videoOutput = qobject_cast<AVFVideoOutput*>(output);

    if (m_videoOutput && state() != QMediaPlayer::StoppedState)
        m_videoOutput->setLayer([(AVFMediaPlayerSessionObserver*)m_observer playerLayer]);
}

void *AVFMediaPlayerSession_Mac::internalAssentHandle()
{
    AVAsset *currentAsset = [[(AVFMediaPlayerSessionObserver*)m_observer playerItem] asset];
    return currentAsset;
}

void AVFMediaPlayerSession_Mac::internalUnloadMedia()
{
    [(AVFMediaPlayerSessionObserver*)m_observer unloadMedia];
}

void AVFMediaPlayerSession_Mac::internalLoadMedia(const QUrl &url)
{
    //Load AVURLAsset
    //initialize asset using content's URL
    NSString *urlString = [NSString stringWithUTF8String:url.toEncoded().constData()];
    NSURL *nsUrl = [NSURL URLWithString:urlString];
    [(AVFMediaPlayerSessionObserver*)m_observer setURL:nsUrl];
}

qint64 AVFMediaPlayerSession_Mac::internalPosition() const
{
    AVPlayerItem *playerItem = [(AVFMediaPlayerSessionObserver*)m_observer playerItem];

    if (!playerItem)
        return 0;

    CMTime time = [playerItem currentTime];
    return static_cast<quint64>(float(time.value) / float(time.timescale) * 1000.0f);
}

qint64 AVFMediaPlayerSession_Mac::internalDuration() const
{
    AVPlayerItem *playerItem = [(AVFMediaPlayerSessionObserver*)m_observer playerItem];

    if (!playerItem)
        return 0;

    CMTime time = [playerItem duration];
    return static_cast<quint64>(float(time.value) / float(time.timescale) * 1000.0f);
}

QMediaTimeRange AVFMediaPlayerSession_Mac::internalPlaybackRanges() const
{
    AVPlayerItem *playerItem = [(AVFMediaPlayerSessionObserver*)m_observer playerItem];

    if (playerItem) {
        QMediaTimeRange timeRanges;

        NSArray *ranges = [playerItem loadedTimeRanges];
        for (NSValue *timeRange in ranges) {
            CMTimeRange currentTimeRange = [timeRange CMTimeRangeValue];
            qint64 startTime = qint64(float(currentTimeRange.start.value) / currentTimeRange.start.timescale * 1000.0);
            timeRanges.addInterval(startTime, startTime + qint64(float(currentTimeRange.duration.value) / currentTimeRange.duration.timescale * 1000.0));
        }
        if (!timeRanges.isEmpty())
            return timeRanges;
    }
    return QMediaTimeRange(0, duration());
}

void AVFMediaPlayerSession_Mac::internalSetRate(qreal value)
{
    AVPlayer *player = [(AVFMediaPlayerSessionObserver*)m_observer player];

    if (player != 0) {
        [player setRate:value];
    }
}

void AVFMediaPlayerSession_Mac::internalSetPosition(qint64 value)
{
    AVPlayerItem *playerItem = [(AVFMediaPlayerSessionObserver*)m_observer playerItem];

    if (!playerItem)
        return;

    CMTime newTime = [playerItem currentTime];
    newTime.value = (value / 1000.0f) * newTime.timescale;
    [playerItem seekToTime:newTime];
}

void AVFMediaPlayerSession_Mac::internalPlay()
{
    if (m_videoOutput) {
        m_videoOutput->setLayer([(AVFMediaPlayerSessionObserver*)m_observer playerLayer]);
    }
    [[(AVFMediaPlayerSessionObserver*)m_observer player] play];
}

void AVFMediaPlayerSession_Mac::internalPause()
{
    if (m_videoOutput) {
        m_videoOutput->setLayer([(AVFMediaPlayerSessionObserver*)m_observer playerLayer]);
    }
    [[(AVFMediaPlayerSessionObserver*)m_observer player] pause];
}

void AVFMediaPlayerSession_Mac::internalStop()
{
    [[(AVFMediaPlayerSessionObserver*)m_observer player] setRate:0.0];
    if (m_videoOutput) {
        m_videoOutput->setLayer(0);
    }
}

void AVFMediaPlayerSession_Mac::internalSetVolume(int value)
{
    AVPlayer *player = [(AVFMediaPlayerSessionObserver*)m_observer player];
    if (player) {
        [[(AVFMediaPlayerSessionObserver*)m_observer player] setVolume:value / 100.0f];
    }
}

void AVFMediaPlayerSession_Mac::internalSetMuted(bool value)
{
    [[(AVFMediaPlayerSessionObserver*)m_observer player] setMuted:value];
}

bool AVFMediaPlayerSession_Mac::internalProcessLoadStateChange()
{
    AVPlayerStatus currentStatus = [[(AVFMediaPlayerSessionObserver*)m_observer player] status];

    if (currentStatus == AVPlayerStatusReadyToPlay) {
        AVPlayerItem *playerItem = [(AVFMediaPlayerSessionObserver*)m_observer playerItem];
        if (playerItem) {
            // Check each track for audio and video content
            AVAssetTrack *videoTrack = nil;
            NSArray *tracks =  playerItem.tracks;
            for (AVPlayerItemTrack *track in tracks) {
                AVAssetTrack *assetTrack = track.assetTrack;
                if (assetTrack) {
                    if ([assetTrack.mediaType isEqualToString:AVMediaTypeAudio])
                        setAudioAvailable(true);
                    if ([assetTrack.mediaType isEqualToString:AVMediaTypeVideo]) {
                        setVideoAvailable(true);
                        if (!videoTrack)
                            videoTrack = assetTrack;
                    }
                }
            }

            // Get the native size of the video, and reset the bounds of the player layer
            AVPlayerLayer *playerLayer = [(AVFMediaPlayerSessionObserver*)m_observer playerLayer];
            if (videoTrack && playerLayer) {
                playerLayer.bounds = CGRectMake(0.0f, 0.0f,
                                                videoTrack.naturalSize.width,
                                                videoTrack.naturalSize.height);

                if (m_videoOutput && state() != QMediaPlayer::StoppedState) {
                    m_videoOutput->setLayer(playerLayer);
                }
            }
        }

        if (state() == QMediaPlayer::PlayingState && [(AVFMediaPlayerSessionObserver*)m_observer player]) {
            [[(AVFMediaPlayerSessionObserver*)m_observer player] setRate:playbackRate()];
            [[(AVFMediaPlayerSessionObserver*)m_observer player] play];
        }

        return true;
    }

    return false;
}
