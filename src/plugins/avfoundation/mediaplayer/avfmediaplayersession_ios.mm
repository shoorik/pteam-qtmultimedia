/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui/QImage>
#include "avfmediaplayersession_ios.h"
#include "avfvideoassetoutput.h"
#include "avfvideosampleprovider_ios.h"

#import <AVFoundation/AVFoundation.h>

QT_USE_NAMESPACE

//AVAsset Keys
static NSString* const AVF_TRACKS_KEY       = @"tracks";
static NSString* const AVF_READABLE_KEY     = @"readable";
static NSString* const AVF_DURATION_KEY     = @"duration";

@interface AVFMediaAssetReader : NSObject
{
@private
    AVFMediaPlayerSession *m_session;
    NSURL *m_URL;
    AVAssetReader* m_videoReader;
    AVAssetReaderTrackOutput* m_videoOutput;
}

@property (readonly, getter=videoOutput) AVAssetReaderTrackOutput* m_videoOutput;
@property (readonly, getter=videoReader) AVAssetReader* m_videoReader;

- (AVFMediaAssetReader *) initWithMediaPlayerSession:(AVFMediaPlayerSession *)session;
- (void) setURL:(NSURL *)url;
- (void) prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys;
- (void) dealloc;
@end


@implementation AVFMediaAssetReader

@synthesize m_videoOutput;

- (AVFMediaAssetReader *) initWithMediaPlayerSession:(AVFMediaPlayerSession *)session
{
    if (!(self = [super init]))
        return nil;

    self->m_session = session;
    return self;
}

- (void) setURL:(NSURL *)url
{
    if (m_URL != url)
    {
        [m_URL release];
        m_URL = [url copy];

        //Create an asset for inspection of a resource referenced by a given URL.
        //Load the values for the asset keys "tracks", "playable".

        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:m_URL options:nil];
        NSArray *requestedKeys = [NSArray arrayWithObjects:AVF_TRACKS_KEY, AVF_READABLE_KEY, AVF_DURATION_KEY, nil];

        // Tells the asset to load the values of any of the specified keys that are not already loaded.
        [asset loadValuesAsynchronouslyForKeys:requestedKeys completionHandler:
         ^{
             dispatch_async( dispatch_get_main_queue(),
                           ^{
                                [self prepareToPlayAsset:asset withKeys:requestedKeys];
                            });
         }];
    }
}

- (void) unloadMedia
{
    if (m_videoReader)
    {
        [m_videoReader release];
    }

    if (m_videoOutput)
    {
        [m_videoOutput release];
        m_videoOutput = 0;
        QMetaObject::invokeMethod(m_session, "playbackStarted", Qt::AutoConnection);
    }
}

- (void) prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
    //Make sure that the value of each key has loaded successfully.
    for (NSString *thisKey in requestedKeys)
    {
        NSError *error = nil;
        AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
        if (keyStatus == AVKeyValueStatusFailed)
        {
            [self assetFailedToPrepareForPlayback:error];
            return;
        }
    }

    if (!asset.readable)
    {
        //Generate an error describing the failure.
        NSString *localizedDescription = NSLocalizedString(@"Item cannot be read", @"Item cannot be read description");
        NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made readable.", @"Item cannot be read failure reason");
        NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
                localizedDescription, NSLocalizedDescriptionKey,
                localizedFailureReason, NSLocalizedFailureReasonErrorKey,
                nil];
        NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];

        [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];

        return;
    }

    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA], (NSString*)kCVPixelBufferPixelFormatTypeKey, nil];

    m_videoReader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
    m_videoOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:[tracks objectAtIndex:0] outputSettings:settings];
    [m_videoReader addOutput:m_videoOutput];
    [m_videoReader startReading];

    QMetaObject::invokeMethod(m_session, "playbackStarted", Qt::AutoConnection);
}

- (void) assetFailedToPrepareForPlayback:(NSError *)error
{
    Q_UNUSED(error)
    QMetaObject::invokeMethod(m_session, "processMediaLoadError", Qt::AutoConnection);
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
    qDebug() << [[error localizedDescription] UTF8String];
    qDebug() << [[error localizedFailureReason] UTF8String];
    qDebug() << [[error localizedRecoverySuggestion] UTF8String];
#endif
}

- (void) dealloc
{
    [self unloadMedia];

    m_session = 0;
    if (m_URL) {
        [m_URL release];
    }

    [super dealloc];
}


@end

AVFMediaPlayerSession_iOS::AVFMediaPlayerSession_iOS(AVFMediaPlayerService *service, QObject *parent)
    : AVFMediaPlayerSession(service, parent)
    , m_videoOutput(0)
{
    m_reader = [[AVFMediaAssetReader alloc] initWithMediaPlayerSession:this];
    m_provider = new AVFVideoSampleProvider_iOS();
}

AVFMediaPlayerSession_iOS::~AVFMediaPlayerSession_iOS()
{
    [(AVFMediaAssetReader*)m_reader release];
    delete m_provider;
}

void AVFMediaPlayerSession_iOS::playbackStarted()
{
    m_provider->setOutput([m_reader videoOutput]);
}

void AVFMediaPlayerSession_iOS::internalSetVideoOutput(QObject *output)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO << output;
#endif

    if (m_videoOutput == qobject_cast<AVFVideoAssetOutput*>(output))
        return;

    //Set the current output layer to null to stop rendering
    if (m_videoOutput) {
        m_videoOutput->setSampleProvider(0);
    }

    m_videoOutput = qobject_cast<AVFVideoAssetOutput*>(output);

    if (m_videoOutput/* && state() != QMediaPlayer::StoppedState*/)
    {
        m_videoOutput->setSampleProvider(m_provider);
    }
}

void *AVFMediaPlayerSession_iOS::internalAssentHandle()
{
    return [[(AVFMediaAssetReader*)m_reader videoReader] asset];
}

void AVFMediaPlayerSession_iOS::internalUnloadMedia()
{
    if (m_videoOutput) {
        m_videoOutput->setSampleProvider(0);
    }
    [(AVFMediaAssetReader*)m_reader unloadMedia];
}

void AVFMediaPlayerSession_iOS::internalLoadMedia(const QUrl &url)
{
    if (m_videoOutput) {
        m_videoOutput->setSampleProvider(m_provider);
    }
    NSString *urlString = [NSString stringWithUTF8String:url.toEncoded().constData()];
    NSURL *nsUrl = [NSURL URLWithString:urlString];
    [(AVFMediaAssetReader*)m_reader setURL:nsUrl];
}

qint64 AVFMediaPlayerSession_iOS::internalPosition() const
{
//    CMSampleBufferRef sampleBuffer = m_provider->readNextSample();

//    if(sampleBuffer == 0) {
//        return 0;
//    }

//    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);

//    CVPixelBufferLockBaseAddress(imageBuffer,0);

//    unsigned char *pixels = ( unsigned char *)CVPixelBufferGetBaseAddress(imageBuffer);
//    int width = CVPixelBufferGetWidth(imageBuffer);
//    int height = CVPixelBufferGetHeight(imageBuffer);
//    int bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);


//    QImage frameData(pixels, width, height, bytesPerRow, QImage::Format_ARGB32);
//    QImage data = frameData;
//    data.detach();

//    if (data.isNull()) {
//        qWarning("renterLayerToImage failed");
//        return 0;
//    }

//    m_videoOutput->present(data);

//    CFRelease(sampleBuffer);

    CMTime time = m_provider->lastTime();
    return 1000 * time.value / time.timescale;
}

qint64 AVFMediaPlayerSession_iOS::internalDuration() const
{
    AVAsset *asset = [[(AVFMediaAssetReader*)m_reader videoReader] asset];

    if (!asset)
        return 0;

    CMTime time = [asset duration];
    return static_cast<quint64>(float(time.value) / float(time.timescale) * 1000.0f);
}

QMediaTimeRange AVFMediaPlayerSession_iOS::internalPlaybackRanges() const
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return QMediaTimeRange(0, 0);
}

void AVFMediaPlayerSession_iOS::internalSetRate(qreal value)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalSetPosition(qint64 value)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalPlay()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalPause()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalStop()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalSetVolume(int value)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

void AVFMediaPlayerSession_iOS::internalSetMuted(bool value)
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
}

bool AVFMediaPlayerSession_iOS::internalProcessLoadStateChange()
{
#ifdef QT_DEBUG_AVF
    qDebug() << Q_FUNC_INFO;
#endif
    return false;
}

