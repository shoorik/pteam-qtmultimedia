/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "avfscreenlink.h"
#include <QtCore/qcoreapplication.h>

#include <QtCore/QDebug>
#include <Foundation/Foundation.h>
#include <QuartzCore/CADisplayLink.h>

QT_USE_NAMESPACE

@interface AVFScreenLinkWrapper : NSObject
{
@private
    AVFScreenLink *m_caller;
    CADisplayLink *m_link;
}

- (AVFScreenLinkWrapper *) initWithCaller:(AVFScreenLink*)caller;
- (void) handler:(CADisplayLink *)link;
- (CFTimeInterval) timestamp;
@end

@implementation AVFScreenLinkWrapper

- (AVFScreenLinkWrapper *) initWithCaller:(AVFScreenLink*)caller;
{
    if (!(self = [super init]))
        return nil;

    self->m_caller = caller;
    self->m_link = [CADisplayLink displayLinkWithTarget:self selector:@selector(handler:)];
    self->m_link.paused = YES;
    [self->m_link addToRunLoop:[NSRunLoop currentRunLoop]
                         forMode:NSDefaultRunLoopMode];

    return self;
}

-(void)dealloc
{
    [m_link invalidate];
    [m_link release];
    [super dealloc];
}

- (bool)isValid
{
    return true;
}

- (bool)isActive
{
     return !m_link.paused;
}

- (void)start
{
    if (m_link && m_link.paused)
    {
        m_link.paused = NO;
    }
}

- (void)stop
{
    if (m_link && !m_link.paused)
    {
        m_link.paused = YES;
    }
}

- (void)handler:(CADisplayLink *)link
{
   m_caller->displayLinkEvent();
}

- (CFTimeInterval)timestamp
{
    return m_link.timestamp;
}
@end

AVFScreenLink::AVFScreenLink(QObject *parent)
    : QObject(parent)
    , m_pendingDisplayLinkEvent(false)
{
    m_wrapper = [[AVFScreenLinkWrapper alloc] initWithCaller:this];
}

AVFScreenLink::~AVFScreenLink()
{
    stop();
    [(id)m_wrapper release];
    m_wrapper = NULL;
}

bool AVFScreenLink::isValid() const
{
    return [(id)m_wrapper isValid];
}

bool AVFScreenLink::isActive() const
{
    return [(id)m_wrapper isActive];
}

void AVFScreenLink::start()
{
    [(id)m_wrapper start];
}

void AVFScreenLink::stop()
{
    [(id)m_wrapper stop];
}


void AVFScreenLink::displayLinkEvent()
{
//     // This function is called from a
//     // thread != gui thread. So we post the event.
//     // But we need to make sure that we don't post faster
//     // than the event loop can eat:
    m_displayLinkMutex.lock();
    bool pending = m_pendingDisplayLinkEvent;
    m_pendingDisplayLinkEvent = true;
    m_displayLinkMutex.unlock();

    if (!pending)
        qApp->postEvent(this, new QEvent(QEvent::User), Qt::HighEventPriority);
}

bool AVFScreenLink::event(QEvent *event)
{
    switch (event->type()){
        case QEvent::User:  {
                m_displayLinkMutex.lock();
                m_pendingDisplayLinkEvent = false;
                m_displayLinkMutex.unlock();
                Q_EMIT tick(1000 * [(id)m_wrapper timestamp]);

                return false;
            }
            break;
        default:
            break;
    }
    return QObject::event(event);
}
