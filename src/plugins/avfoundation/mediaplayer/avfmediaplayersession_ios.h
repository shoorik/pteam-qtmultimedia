/****************************************************************************
**
** Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL21$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia. For licensing terms and
** conditions see http://qt.digia.com/licensing. For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 or version 3 as published by the Free
** Software Foundation and appearing in the file LICENSE.LGPLv21 and
** LICENSE.LGPLv3 included in the packaging of this file. Please review the
** following information to ensure the GNU Lesser General Public License
** requirements will be met: https://www.gnu.org/licenses/lgpl.html and
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights. These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef AVFMEDIAPLAYERSESSION_IOS_H
#define AVFMEDIAPLAYERSESSION_IOS_H

#include "avfmediaplayersession.h"
#include <QtMultimedia/QMediaPlayerControl>
#include <QtMultimedia/QMediaPlayer>

QT_BEGIN_NAMESPACE

class AVFMediaPlayerService;
class AVFVideoAssetOutput;
class AVFVideoSampleProvider_iOS;

class AVFMediaPlayerSession_iOS : public AVFMediaPlayerSession
{
    Q_OBJECT

public:
    AVFMediaPlayerSession_iOS(AVFMediaPlayerService *service, QObject *parent = 0);
    virtual ~AVFMediaPlayerSession_iOS();

protected Q_SLOTS:
    void playbackStarted();

protected:
    virtual void internalSetVideoOutput(QObject *output);
    virtual void *internalAssentHandle();
    virtual void internalUnloadMedia();
    virtual void internalLoadMedia(const QUrl &url);
    virtual qint64 internalPosition() const;
    virtual qint64 internalDuration() const;
    virtual QMediaTimeRange internalPlaybackRanges() const;
    virtual void internalSetRate(qreal value);
    virtual void internalSetPosition(qint64 value);
    virtual void internalPlay();
    virtual void internalPause();
    virtual void internalStop();
    virtual void internalSetVolume(int value);
    virtual void internalSetMuted(bool value);
    virtual bool internalProcessLoadStateChange();

private:
    void* m_reader;
    AVFVideoAssetOutput* m_videoOutput;
    AVFVideoSampleProvider_iOS* m_provider;

};

QT_END_NAMESPACE

#endif // AVFMEDIAPLAYERSESSION_IOS_H
